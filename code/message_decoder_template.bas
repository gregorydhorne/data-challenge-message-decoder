1 REM  #########################################################################
2 REM  #                                                                       #
3 REM  # Script Name: message_decoder.bas                                      #
4 REM  # Creator: Gregory D. Horne (greg at gregoryhorne dot ca)               #
5 REM  #                                                                       #
6 REM  # Data Provider: Clincial Developer Club                                #
7 REM  # Data Provider's Website: http://clinicaldevelopers.org                #
8 REM  #                                                                       #
9 REM  # Interpreter: Applesoft BASIC in Javascript                            #
10 REM # URL: http://www.calormen.com/jsbasic/                                 #
11 REM #                                                                       #
12 REM #########################################################################
13 REM
14 REM Read FASTA DNA sequence and display the identifer.
15 REM
16 PRINT "DNA DECODER"
17 READ ID$
18 PRINT "ID:" + MID$(ID$, 2, 8)
19 REM
20 REM Continue reading the FASTA DNA sequence which contains the bases.
21 REM
22 DIM DNASEQ$(2048):LET I = 0
23 ONERR GOTO 30
24 READ DNA$
25 I = I + 1:DNASEQ$(I) = DNA$
26 GOTO 23
27 REM
28 REM Assemble the DNA sequence into a single string to simplify processing.
29 REM
30 LET DNA$ = ""
31 FOR J = 1 TO I
32 DNA$ = DNA$ + DNASEQ$(J)
33 NEXT J
34 PRINT "Sequence Length: " + STR$(LEN(DNA$))
35 REM
36 REM Convert DNA bases (A, C, G, T) to their binary representation.
37 REM (A : 0, C : 0, G: 1, T : 1) so the hidden message can be found, where
38 REM each eight-bit string represents an ASCII character.
39 REM
40 FOR I = 1 TO LEN(DNA$)
41 IF MID$(DNA$, I, 1) = "A" OR MID$(DNA$, I, 1) = "C" THEN DNA$ = LEFT$(DNA$, I - 1) + "0" + RIGHT$(DNA$, LEN(DNA$) - I)
42 IF MID$(DNA$, I, 1) = "G" OR MID$(DNA$, I, 1) = "T" THEN DNA$ = LEFT$(DNA$, I - 1) + "1" + RIGHT$(DNA$, LEN(DNA$) - I)
43 NEXT I
44 REM
45 REM Add ASCII 0 (zero) characters to the beeginning of the DNA sequence
46 REM making  its length a multiple of 8 (8 bits equals 1 byte in ASCII
47 REM alphabet).
48 REM
49 PADDING = (LEN(DNA$) - (LEN(DNA$) / 8) * 8) * 100:IF PADDING < 1 THEN 63
50 FOR I = 1 TO PADDING - 1
51 DNA$="0" + DNA$
52 NEXT I:PRINT "Sequence Length: " + STR$(LEN(DNA$))
53 REM
54 REM Translate DNA sequence in its binary equivalent form to its ASCII
55 REM decoded representation. For each iteration adjust the offset of the
56 REM starting position by 0, 1, 2, 3, 4, 5, and 6, simulating how the beginning
57 REM of a DNA base-pair is determined. ASCII characters between 32 and 126
58 REM inclusive are the printable members of the ASCII alphabet, while ASCII
59 REM characters between 0 and 31 along with 127 inclusive are the
60 REM non-printable members, and ASCII characters between 128 and 255
61 REM inclusive are printable graphic characters.
62 REM
63 PRINT "DNA Sequence:": PRINT DNA$
64 FOR FRAME = 0 TO 6
65 PRINT:PRINT "Frameshift: " + STR$(FRAME):PRINT
66 FOR I = 0 TO LEN(DNA$) STEP 8
67 LET BITSTRING$ = MID$(DNA$, FRAME + I + 1, 8):LET BYTE = 0
68 FOR BIT = 7 TO 0 STEP -1 
69 BYTE = BYTE + VAL(MID$(BITSTRING$, 7 - BIT + 1, 1)) * 2 ^ BIT
70 NEXT BIT
71 IF BYTE > 7 AND BYTE < 14 OR BYTE > 31 AND BYTE < 128 THEN PRINT CHR$(BYTE);
72 NEXT I
73 NEXT FRAME
74 PRINT:PRINT:PRINT "Completed":PRINT
75 END
