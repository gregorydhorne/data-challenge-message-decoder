#!/usr/bin/env sh

###############################################################################
#                                                                             #
# Script Name: message_decoder.sh                                             #
# Creator: Gregory D. Horne (greg at gregoryhorne dot ca)                     #
#                                                                             #
# Data Provider: Clincial Developer Club                                      #
# Data Provider's Website: http://clinicaldevelopers.org                      #
#                                                                             #
###############################################################################

# Read DNA FASTA sequence file.

while read line
do
    fasta=${fasta}${line}
done < ./data/id100228968.seq

# Extract the FASTA sequence identifier and determine DNA sequence length.

id=`echo ${fasta} | cut -d \> -f 2 | cut -d ' ' -f 1`
echo "ID: ${id}"

dna=`echo ${fasta} | cut -d \> -f 3`
echo "Sequence Length: ${#dna}"

# Convert DNA bases (A, C, G, T) to their binary (0, 1) representation
# (A : 0, C : 0, G: 1, T : 1) so the hidden message can be found, where each
# eight-bit string represents an ASCII character.

bit_string=`echo ${dna} | sed 's/[A|C]/0/g;s/[G|T]/1/g'`

# Add ASCII 0 (zero) characters to the beginning of the DNA sequence making
# its length a multiple of 8 (8 bits equals 1 byte in ASCII alphabet).

padding=$((${#bit_string} % 8))
bit_string=`head -c ${padding} < /dev/zero | tr "\0" "0"`${bit_string}

# Translate DNA sequence in its binary equivalent form to its ASCII decoded
# representation. For each iteration adjust the offset of the starting position
# by 0, 1, 2, 3, 4, 5, and 6, simulating how the beginning of a DNA base-pair
# is determined. ASCII characters between 32 and 126 inclusive are printable
# members of the ASCII alphabet, while ASCII characters between 0 and 31 along
# with 127 inclusive are the non-printable members, and ASCII characters between
# 128 and 255 inclusive are printable graphic characters.

echo
echo "DNA Sequence:"
echo ${bit_string}

for frame in `seq 0 6`
do
    echo
    echo
    echo "Frameshift: ${frame}"
    echo

    for bit in `seq 0 8 ${#bit_string}`
    do
        #echo ${bit_string:$((frame_bit)):8}; exit 0
        byte=$(echo "ibase=2; ${bit_string:$((frame+bit)):8}" | bc)
        if [ ! -z ${byte} ]
	then
            if [ ${byte} -gt 7 ] && [ ${byte} -lt 14 ] || [ ${byte} -gt 31 ] && [ ${byte} -lt 128 ]
            then
	        printf "\x$(printf %x ${byte})"
            fi
	fi
    done
done

echo
echo
echo "Completed"
echo

exit 0
