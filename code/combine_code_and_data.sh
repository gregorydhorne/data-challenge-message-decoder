#~/usr/bin/env sh

###############################################################################
#                                                                             #
# Script Name: combine_code_and_data.sh                                       #
# Creator: Gregory D. Horne (greg at gregoryhorne dot ca)                     #
#                                                                             #
# Data Provider: Clincial Developer Club                                      #
# Data Provider's Website: http://clinicaldevelopers.org                      #
#                                                                             #
###############################################################################

# Concatemate code and data files.

cat ./code/message_decoder_template.bas ./data/dna.dat \
  > ./code/message_decoder.bas

exit 0
