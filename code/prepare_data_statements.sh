#!/usr/bin/env sh

###############################################################################
#                                                                             #
# Script Name: prepare_data_statements.sh                                     #
# Creator: Gregory D. Horne (greg at gregoryhorne dot ca)                     #
#                                                                             #
# Data Provider: Clincial Developer Club                                      #
# Data Provider's Website: http://clinicaldevelopers.org                      #
#                                                                             #
###############################################################################

# Read DNA FASTA sequence file and generate "DATA" statements for BASIC.

temp_file=$(mktemp)

awk '{ print "\""$0"\""}' ./data/id100228968.seq > ${temp_file}

sed -i -e 's/^/DATA /' ${temp_file}
nl \
    --starting-line-number 1000 \
    --no-renumber \
    --number-format=ln \
    --number-separator="" \
${temp_file} > ./data/dna.dat

rm ${temp_file}

exit 0
